# Release Channels

####  Start the build

1. android แก้ไข version ใน  C:\u_set_driver\app.json
2. สั่ง build

`ex.  expo build:android  --release-channel=dev --config C:\u_set_driver\app.json`

ref. https://docs.expo.io/versions/v34.0.0/distribution/building-standalone-apps/

#### Start the publishes

1.publishes แก้ไข detail ใน  C:\u_set_driver\app.json

`ex. expo publish --release-channel=prd --config C:\u_set_driver\app.json`

2.See past publishes

`ex. expo publish:history`

3.To see more details about this particular release 

`ex. expo publish:details --publish-id d9bd6b80`

ref. https://docs.expo.io/versions/v34.0.0/distribution/release-channels/

ref. https://docs.expo.io/versions/v34.0.0/distribution/advanced-release-channels/

# IPA Plist

itms-services://?action=download-manifest&url=https://gitlab.com/uinfo_public/ipa_dist/raw/master/com.uinfo.usetdriver/ios/manifest_prd.plist?inline=false