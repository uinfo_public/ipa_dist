# ipa_dist

unzip Foobar.ipa
security cms -Di Payload/Foobar.app/embedded.mobileprovision

# Resign

expo credentials:manager

expo build:ios --clear-provisioning-profile

security find-identity -p codesigning

```
xresign.sh \
-s /Users/minkbear/Work_SRC/ipa_dist/com.uinfo.uappsm2/UAppsM2_3.0.25_prd_sdk_39.ipa \
-c "iPhone Distribution: U INFO COMPANY LIMITED" \
-p /Users/minkbear/Work_SRC/ipa_dist/ios_cert/uappsm2_exp20230704_S2LQ9XTSQ3.mobileprovision
```

```
xresign.sh \
-s /Users/minkbear/Work_SRC/ipa_dist/com.uinfo.usetdriver/ios/usetdriver_0.2.0_prd.ipa \
-c "iPhone Distribution: U INFO COMPANY LIMITED" \
-p /Users/minkbear/Work_SRC/ipa_dist/ios_cert/usetdriver_exp20230829_H77C94XKV9.mobileprovision
```

# Expire Info

uappsm2     04/08/2023
usetdriver  29/08/2023  10/09/2022
uappsmr     18/11/2022
