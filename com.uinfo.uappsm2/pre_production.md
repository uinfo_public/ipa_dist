# VERSION 3.0.20 (pre_production)

## ANDROID

- [Download](https://gitlab.com/uinfo_public/ipa_dist/-/raw/master/com.uinfo.uappsm2/UAppsM2_3.0.20_pre_prd_sdk_39.apk?inline=false)

## IOS

- ![pre_production 3.0.20](https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=itms-services%3A%2F%2F%3Faction%3Ddownload-manifest%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fuinfo_public%2Fipa_dist%2Fraw%2Fmaster%2Fcom.uinfo.uappsm2%2Fmanifest_pre_prd_sdk_39.plist)
